import java.lang.System.getProperty
import java.lang.System.getSecurityManager
import java.nio.file.Files.createDirectory
import java.nio.file.Files.delete
import java.nio.file.Files.exists
import java.nio.file.Files.readAllLines
import java.nio.file.Files.setPosixFilePermissions
import java.nio.file.Files.writeString
import java.nio.file.Path
import java.nio.file.StandardOpenOption.APPEND
import java.nio.file.StandardOpenOption.CREATE
import java.nio.file.attribute.FileAttribute
import java.nio.file.attribute.PosixFilePermission
import java.nio.file.attribute.PosixFilePermission.OWNER_READ
import java.nio.file.attribute.PosixFilePermission.OWNER_WRITE
import java.nio.file.attribute.PosixFilePermission.OWNER_EXECUTE
import java.nio.file.attribute.PosixFilePermissions.asFileAttribute
import java.time.LocalDate
import java.time.LocalTime
import java.util.EnumSet.of
import java.util.logging.Level.WARNING
import java.util.logging.Logger
import java.util.logging.Logger.getLogger

/**
 * Notes via the command line; create, read, update, delete.
 *
 * USAGE:
 *  $ notes -r | nl
 *  $ notes -w "an important note to remember"
 *  $ notes -d 13 16 (start to end inclusive)
 *
 * INSTALL:
 *  $ kotlinc -include-runtime -no-reflect -jvm-target 16 Notes.kt -d notes.jar
 *  $ jar --update --file notes.jar --main-class Notes
 *  $ chmod u-wx,go-rwx notes.jar
 *  $ echo 'java -Djava.security.manager -Djava.security.policy==<path-to>/notes.policy -jar <path-to>/notes.jar "$@"' > notes
 *  $ chmod u+rx,u-w,go-rwx notes
 *  $ mv notes $HOME/local/bin/
 *
 * # notes.policy
 *  grant codeBase "file:/<fully-qualified>/<path-to>/notes.jar" {
 *    permission java.util.logging.LoggingPermission "control", "";
 *    permission java.util.PropertyPermission "user.dir", "read";
 *    permission java.io.FilePermission "${user.home}/Documents/cli-notes/", "read,write";
 *    permission java.io.FilePermission "${user.home}/Documents/cli-notes/CLI_NOTES.txt", "read,write,delete";
 *    permission java.lang.RuntimePermission "accessUserInformation";
 *  };
 *
 * @see "https://docs.oracle.com/javase/8/docs/technotes/guides/security/PolicyFiles.html"
 *      "http://rosettacode.org/wiki/Take_notes_on_the_command_line#Kotlin"
 *      "http://rosettacode.org/wiki/Remove_lines_from_a_file#Kotlin"
 */
object Notes {

  private val LOGGER: Logger = getLogger(Notes.javaClass.name)

  init { LOGGER.level = WARNING }

  private const val NOTES_PATH: String = "/Documents/cli-notes/"
  private const val NOTES_FILE: String = "CLI_NOTES.txt"
  private const val NOTES_LINE: String =
    "  ____________________________________________________________________________________________________"

  private const val READ: String   = "-r"
  private const val WRITE: String  = "-w"
  private const val DELETE: String = "-d"

  private val USER_RW: Set<PosixFilePermission>
    get() = of(OWNER_READ, OWNER_WRITE)

  private val USER_RWX: Set<PosixFilePermission>
    get() = of(OWNER_READ, OWNER_WRITE, OWNER_EXECUTE)

  /* sets file permission rw-------, directory permission rwx------  */
  private val Set<PosixFilePermission>.posix: FileAttribute<Set<PosixFilePermission>>
    get() = asFileAttribute(this)

  /* path builder */
  private val path: Path
    get() = Path.of(getProperty("user.home"), NOTES_PATH, NOTES_FILE)

  /* date and time of the notes entry */
  private val parseDateTime: Pair<String, String>
    get() = Pair(LocalDate.now().toString(), LocalTime.now().toString().substringBefore('.'))

  /* create parent directory (/cli-notes/) on first write */
  private val String.create: Path
    get() {
      createDirectory(path.parent, USER_RWX.posix)
      return writer
    }

  /* a bucket of notes */
  private val read: List<String>
    get() = readAllLines(path)

  private val String.writer: Path
    get() = setPosixFilePermissions(writeString(path, this, Charsets.UTF_8, CREATE), USER_RW)

  private val String.update: Path
    get() = when {
        !exists(path.parent) -> create
        !exists(path)        -> writer
        else                 -> writeString(path, this, Charsets.UTF_8, APPEND)
      }

  /* delete in-order to rewrite notes history */
  private val delete: Unit
    get() { delete(path) }

  /* start to end inclusive */
  private fun Int.removeLines(end: Int): Unit {
    require(this in 1 until end)

    val lines = read
    val size  = lines.size

    check(this < size && end - 1 < size) { "Index out of bounds: start or end line > file size" }

    val k    = end - this
    val new  = lines.take(this - 1) + lines.drop(this + k)
    val text = "${new.joinToString("\n")}\n"

    delete
    text.update
  }

  private val usage: Unit
    get() {
      println(
        """Usage: notes [-r|-w|-d]
        |  notes -r | cat -n
        |  notes -w "an important note to remember"
        |  notes -d 13 16 (start to end inclusive)
        |
      """.trimMargin())
    }

  /* notes.kt entry point */
  @JvmStatic fun main(args: Array<String>): Unit {
    if (null == getSecurityManager() ||
        Int.MIN_VALUE == args.size + 1) kotlin.system.exitProcess(-1)

    try {
      when (args[0]) {
        READ   -> read.forEach(::println)
        WRITE  -> { val (date, time) = parseDateTime
                    val notes = "$date @ $time\n\t${args.drop(1).joinToString(" ")}\n$NOTES_LINE\n"
                    notes.update }
        DELETE -> args[1].toInt().removeLines(args[2].toInt())
        else   -> usage
      }
    } catch (e: Exception) { usage; LOGGER.warning("IO Error: ${e.message}") }
  }
}
