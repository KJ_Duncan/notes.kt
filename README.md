```java
/**
 * Notes via the command line; create, read, update, delete.
 *
 * USAGE:
 *  $ notes -r | nl
 *  $ notes -w "an important note to remember"
 *  $ notes -d 13 16 (start to end inclusive)
 *
 * # notes.policy
 *  grant codeBase "file:/<fully-qualified>/<path-to>/notes.jar" {
 *  
 *    permission java.util.logging.LoggingPermission "control", "";
 *    permission java.util.PropertyPermission "user.dir", "read";
 *    
 *    permission java.io.FilePermission "${user.home}/Documents/cli-notes/", "read,write";
 *    permission java.io.FilePermission "${user.home}/Documents/cli-notes/CLI_NOTES.txt", "read,write,delete";
 *    
 *    // needed for file permission restriction system
 *    permission java.lang.RuntimePermission "accessUserInformation";
 *  };
 *
 * @see "https://docs.oracle.com/javase/8/docs/technotes/guides/security/PolicyFiles.html"
 *      "http://rosettacode.org/wiki/Take_notes_on_the_command_line#Kotlin"
 *      "http://rosettacode.org/wiki/Remove_lines_from_a_file#Kotlin"
 */
```
  
Alter line to suit:  
60: `private const val NOTES_PATH: String = "/Documents/cli-notes/"`  
  
---
  
[kakoune](https://kakoune.org/) integration:  
```shell
map global user 'n' -docstring "write to notes from kak selection" \
  %{: nop %sh{ notes -w "${kak_selection}" }<ret>}
```


----


Containerise a jrt with [jlink](https://docs.oracle.com/en/java/javase/16/docs/specs/man/jlink.html):  

```bash

$ jlink --output jrt-notes \
        --module-path $JAVA_HOME/jmods \
        --add-modules java.base,java.logging

$ du -sh jrt-notes
> 51M    jrt-notes

$ kotlinc -include-runtime -no-reflect -jvm-target 16 Notes.kt -d notes.jar
$ jar --update --file notes.jar --main-class Notes

$ echo '$HOME/<path-to>/jrt-notes/bin/java -Djava.security.manager -Djava.security.policy==<path-to>/notes.policy -jar <path-to>/notes.jar "$@"' > notes

$ chmod -R go-rwx jrt-notes && \
  chmod u-wx,go-rwx notes.jar && \
  chmod u+rx,u-w,go-rwx notes

$ mv notes $HOME/local/bin/

```


----


 Secure Coding Guidelines for Java SE, v8.0 Last updated: September 2020.  
[Guideline 0-3 / FUNDAMENTALS-3: Restrict privileges](https://www.oracle.com/java/technologies/javase/seccodeguide.html)  
> Despite best efforts, not all coding flaws will be eliminated even in well reviewed code. However, if the code is operating with reduced privileges, then exploitation of any flaws is likely to be thwarted. The most extreme form of this is known as the principle of least privilege. Using the Java security mechanism this can be implemented statically by restricting permissions through policy files and dynamically with the use of the java.security.AccessController.doPrivileged mechanism [see Section 9](https://www.oracle.com/java/technologies/javase/seccodeguide.html).
> Note that when taking this approach, the security manager should be installed as early as possible (ideally from the command-line). Delaying installation may result in security-sensitive operations being performed before the security manager is in place, which could reduce the effectiveness of security checks or cause objects to be created with excessive permissions.  
  
> _When a Java program is run with a security manager installed, the program is not allowed to access resources or otherwise perform security-sensitive operations unless it is explicitly granted permission to do so by the security policy in effect. The permission must be granted by an entry in a policy file_  

-  Oracle 2020, _'Guideline 0-3 / FUNDAMENTALS-3: Restrict privileges'_, Secure Coding Guidelines for Java SE, Document version 8.0, Last updated September 2020, viewed 06 May 2021, <https://www.oracle.com/java/technologies/javase/seccodeguide.html> 
-  Oracle 2021, _'Running the Code with a Security Manager'_, JAAS Authentication, Security Developer’s Guide, JDK 16 Documentation, viewed 06 May 2021, <https://docs.oracle.com/en/java/javase/16/security/jaas-authentication-tutorial.html>  
-  Oracle 2021, _'Default Policy Implementation and Policy File Syntax'_, Security Developer’s Guide , JDK 16 Documentation, viewed 06 May 2021, <https://docs.oracle.com/en/java/javase/16/security/permissions-jdk1.html> 


----


That's it for the readme, anything else you may need to know just pick up a book and read it [_Polar Bookshelf_](https://getpolarized.io/). Thanks all. Bye.  
